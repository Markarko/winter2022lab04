public class Burger{
	private double price;
	private String meatType;
	private String extraCheese;
	
	public Burger(double price, String meatType, String extraCheese){
		this.price = price;
		this.meatType = meatType;
		this.extraCheese = extraCheese;
	}
	public boolean ApplyTax(String essentialFood, double finalPrice){
		double taxes = 1.15;
		if (essentialFood.equals("no")){
			price *= taxes;
			return true;
		}
		return false;
	}
	public double GetPrice(){
		return this.price;
	}
	public String GetMeatType(){
		return this.meatType;
	}
	public String GetExtraCheese(){
		return this.extraCheese;
	}
	public void SetPrice(double price){
		if (price > 0){
			this.price = price;
		}
	}
	public void SetMeatType(String meatType){
		if (meatType.length() > 0){
			this.meatType = meatType;
		}
	}
	public void SetExtraCheese(String extraCheese){
		if (extraCheese.length() > 0){
			this.extraCheese = extraCheese;
		}
	}
}