import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Burger[] burgers = new Burger[4];
		for(int i = 0; i < burgers.length; i++){
			System.out.println((i+1) + " out of " + burgers.length + " entries");
			System.out.print("Please enter the burger price: ");
			
			double price = reader.nextDouble();
			System.out.print("Please enter the meat you want to have in your burger: ");
			
			String meatType = reader.next();
			System.out.print("Would you like extra cheese in your burger (yes/no): ");
			
			String extraCheese = reader.next();
			burgers[i] = new Burger(price, meatType, extraCheese);
			System.out.println("");
		}
		Burger lastBurger = burgers[burgers.length - 1];
		System.out.println("");
		System.out.println("The price of the burger is " + lastBurger.GetPrice() + "$");
		System.out.println("The meat that you chose for you burger is " + lastBurger.GetMeatType());
		if (lastBurger.GetExtraCheese().equals("yes")){
			System.out.println("You chose extra cheese for your burger");
		}
		else {
			System.out.println("You chose to pass on extra cheese, how dare you...");
		}
		System.out.print("Enter a new price for your burger: ");
		double price = reader.nextDouble();
		lastBurger.SetPrice(price);
		
		System.out.print("Enter a new meat type for your burger: ");
		String meatType = reader.next();
		lastBurger.SetMeatType(meatType);
		
		System.out.print("Do you want some extra cheese on your burger? (yes/no): ");
		String extraCheese = reader.next();
		lastBurger.SetExtraCheese(extraCheese);
		
		System.out.println("The new price for your burger is: " + price + "$");
		System.out.println("The new meat type for your burger is: " + meatType);
		System.out.println("");
		
		if (lastBurger.GetExtraCheese().equals("yes")){
			System.out.println("You chose extra cheese for your burger");
		}
		else {
			System.out.println("You chose to pass on extra cheese, how dare you...");
		}
		
		System.out.print("Do you consider burgers as essential food? (yes/no): ");
		String essentialFood = reader.next();
		if (lastBurger.ApplyTax(essentialFood, lastBurger.GetPrice())){
			System.out.println("Your burger was taxed :(");
			System.out.println("The new price is " + lastBurger.GetPrice() + "$");
		}
		else{
			System.out.println("Fine, we won't tax your burger...");
		}
	}
}
